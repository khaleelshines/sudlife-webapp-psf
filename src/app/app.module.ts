import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule ,routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginService } from 'src/services/login.services';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_helpers';
import { RoleGuard } from './_helpers/role-guard';

const myRoots:Routes = [
  {path:"",component:LoginComponent},
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard, RoleGuard]
  },
]

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      myRoots,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }

const myRoutes:Routes = [
  {path: "", component: LoginComponent},
  {path: "login", component: LoginComponent}
]
