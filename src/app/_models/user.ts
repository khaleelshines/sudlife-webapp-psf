
export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    user_type_id?:number;
    organization:any;
    usertoken: string;
    subscribedteam: any;
    userprofile: any;
    data:any;
}