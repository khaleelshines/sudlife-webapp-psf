import { Roles } from 'src/app/_models/roles';

export class ApiResponse {
    Code: string;
    Status: string;
    Data: string;
    Message: string;
    Description: string;
    HelpUrl: string;
    role: Roles;
    token?: string;
}
