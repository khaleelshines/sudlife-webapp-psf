import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from 'src/services/login.services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm: FormGroup;
  invalidLogin = false;
  submitted = false;
  showbutton = false;
  showeye = false;
  ModuleInfo: any[] = [];
  loginResponseObject: any;
  loading = false;
  isSessionExpired:boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, 
    private loginService: LoginService,
    private router: Router) {}
    
  showPassword() {
    this.showbutton = !this.showbutton;
    this.showeye = !this.showeye;
  }
    
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;

    this.loginService.Login(this.loginForm.controls.loginId.value, this.loginForm.controls.password.value)
    .subscribe(data => {
      if (data.code === 'OWAPI-200' && data.status ==='SUCCESS') {
          this.router.navigate(['/dashboard']);
          // if (userInfo.RoleInfo[0].RoleName.toLowerCase() === 'Admin') {
          //   this.router.navigate(['/dashboard']);
          // } else {
          //   this.router.navigate(['/dashboard/member']);
          // }
      } else {
        this.loading = false;
        this.invalidLogin = true;
        this.router.navigate(['/login']);
      }
    });
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      loginId: ['', Validators.compose([Validators.required])],
      password: ['', [Validators.compose([Validators.required,Validators.maxLength(20)])]],
    });

    let loginTime = localStorage.getItem('logintime');
    if(parseInt(loginTime) < 1){
      this.isSessionExpired = true;
       setTimeout(() => {
        this.isSessionExpired = false;
        localStorage.setItem('logintime','1')
       }, 3000);
    }
  }


}
