import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate{ 
  constructor(private auth: AuthenticationService, private _router: Router,
    private authenticationService: AuthenticationService) {
  }
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const currentUser = this.authenticationService.currentUserValue;
      const userRole = currentUser.userprofile.role;
      if(userRole === "supervisor"){
        return true;
      }else{
        this._router.navigate(["access-denied"]);
        return false;
      }
  }
}


