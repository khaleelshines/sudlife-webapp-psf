import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse,HTTP_INTERCEPTORS } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { ApiResponse } from '..//app/_models/apiresponse.model';
import { environment } from '../environments/environment';
import { User } from '../app/_models/user';
import { map } from 'rxjs/operators';

@Injectable()
export class LoginService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

    Login(loginid: string, password :string) {
    return this.http.post<any>(`${environment.apiUrl}/admin/v1/login?loginid=`+loginid+`&password=`+password, null)
    .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.data) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', user.data);      
            this.currentUserSubject.next(user.data);
        }
        return user; 
    }));
  }
 
}
